//
//  Pasien.swift
//  SOSpital
//
//  Created by Arles Halley on 24/08/21.
//

import Foundation

struct Pasien: Identifiable {
    let id: String = UUID().uuidString
    let namaDepan: String
    let namaBelakang: String
    let kamar: String
    let bed: String
    var heartRate: Int
    var respirationRate: Int
    var saturation: Int
    var systolic: Int
    var diastolic: Int
    var statusPasien: String
}
