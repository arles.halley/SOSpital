//
//  SOSpitalApp.swift
//  SOSpital
//
//  Created by Arles Halley on 24/08/21.
//

import SwiftUI

@main
struct SOSpitalApp: App {
    
    @StateObject var pasienViewModel: PasienViewModel = PasienViewModel()
    
    var body: some Scene {
        WindowGroup {
            DaftarPasienView().environmentObject(pasienViewModel)
        }
    }
}
