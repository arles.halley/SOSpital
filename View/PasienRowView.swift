//
//  PasienRowView.swift
//  SOSpital
//
//  Created by Arles Halley on 25/08/21.
//

import SwiftUI

struct PasienRowView: View {
    
    let pasien: Pasien
    
    var body: some View {
        VStack (alignment: .leading, spacing: 5){
            HStack {
                Text("\(pasien.namaDepan) \(pasien.namaBelakang)")
                    .fontWeight(.semibold)
                    .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                Spacer()
            }
            
            HStack {
                Text("Kamar \(pasien.kamar) Bed \(pasien.bed)")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                Spacer()
            }
        }
    }
}

struct PasienRowView_Previews: PreviewProvider {
    
    static var pasien1 = Pasien(namaDepan: "Alpha", namaBelakang: "Omega", kamar: "1234", bed: "A", heartRate: 90, respirationRate: 18, saturation: 98, systolic: 120, diastolic: 80, statusPasien: "Normal")
    static var pasien2 = Pasien(namaDepan: "Omega", namaBelakang: "Alpha", kamar: "4321", bed: "B", heartRate: 90, respirationRate: 18, saturation: 98, systolic: 120, diastolic: 80, statusPasien: "Normal")
    
    static var previews: some View {
        Group {
            PasienRowView(pasien: pasien1)
            PasienRowView(pasien: pasien2)
        }
        .previewLayout(.sizeThatFits)
    }
}
