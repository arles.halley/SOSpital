//
//  InputFormView.swift
//  SOSpital
//
//  Created by Arles Halley on 25/08/21.
//

import SwiftUI

struct InputFormView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var pasienViewModel: PasienViewModel
    @State private var namaDepan = ""
    @State private var namaBelakang = ""
    @State private var kamar = ""
    @State private var bed = ""
    @State private var saturasi = ""
    @State private var heartRate = ""
    @State private var respirationRate = ""
    @State private var sistolik = ""
    @State private var diastolik = ""
    
    var body: some View {
        NavigationView{
            Form {
                Section(header: Text("Data Pasien")) {
                    TextField("Nama depan", text: $namaDepan)
                    TextField("Nama belakang", text: $namaBelakang)
                    TextField("Kamar", text: $kamar)
                    TextField("Bed", text: $bed)
                }
                
                Section(header: Text("Tanda Vital")) {
                    TextField("Heart Rate", text: $heartRate)
                    TextField("SpO2", text: $saturasi)
                    TextField("Respiration Rate", text: $respirationRate)
                    TextField("Systolic", text: $sistolik)
                    TextField("Diastolic", text: $diastolik)
                }
            }
            .navigationTitle("Input")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    Button ("Hapus", action: hapusData)
                    
                }
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button ("Simpan", action: simpanData)
                }
            }
            .onTapGesture {
                hideKeyboard()
            }
        }
        .tabItem {
            Image(systemName: "doc.text.below.ecg")
            Text("Input")
        }
    }
    
    func simpanData() {
        
        let heartRate = Int(heartRate)!
        let saturasi = Int(saturasi)!
        let respirationRate = Int(respirationRate)!
        let sistolik = Int(sistolik)!
        let diastolik = Int(diastolik)!
        var status = "Normal"

        pasienViewModel.tambahPasien(namaDepan: namaDepan, namaBelakang: namaBelakang, kamar: kamar, bed: bed, heartRate: heartRate, respirationRate: respirationRate, saturation: saturasi, systolic: sistolik, diastolic: diastolik, statusPasien: status)
        
        if heartRate < 50 || heartRate > 120 {
            status = "Darurat"
        } else if heartRate >= 50 && heartRate < 60 || heartRate > 100 && heartRate <= 120 {
            status = "Gawat"
        }

        if saturasi < 93 {
            status = "Darurat"
        } else if saturasi >= 93 && saturasi <= 95 {
            status = "Gawat"
        }

        if respirationRate < 10 || respirationRate > 28 {
            status = "Darurat"
        } else if respirationRate >= 10 && respirationRate < 16 || respirationRate > 20 && respirationRate <= 28 {
            status = "Gawat"
        }
        
        if sistolik < 90 || sistolik >= 140 {
            status = "Darurat"
        } else if sistolik >= 90 && sistolik < 120 || sistolik > 120 && sistolik < 140 {
            status = "Gawat"
        }

        if diastolik < 50 || diastolik >= 100 {
            status = "Darurat"
        } else if diastolik >= 50 && diastolik < 70 || diastolik > 90 && diastolik < 100 {
            status = "Gawat"
        }
        
        presentationMode.wrappedValue.dismiss()
        
        self.namaDepan = ""
        self.namaBelakang = ""
        self.kamar = ""
        self.bed = ""
        self.heartRate = ""
        self.saturasi = ""
        self.respirationRate = ""
        self.sistolik = ""
        self.diastolik = ""
        
        print("Data tersimpan")
    }

    func hapusData() {
        self.namaDepan = ""
        self.namaBelakang = ""
        self.kamar = ""
        self.bed = ""
        self.heartRate = ""
        self.saturasi = ""
        self.respirationRate = ""
        self.sistolik = ""
        self.diastolik = ""
        
        print("Data terhapus")
    }
    
}

struct InputFormView_Previews: PreviewProvider {
    static var previews: some View {
        InputFormView()
            .environmentObject(PasienViewModel())
    }
}
