//
//  ContentView.swift
//  SOSpital
//
//  Created by Arles Halley on 24/08/21.
//

import SwiftUI

struct DaftarPasienView: View {
    
    @EnvironmentObject var pasienViewModel: PasienViewModel
    
    var body: some View {
        TabView{
            //Input data pasien
            InputFormView()
            
            //List pasien
            NavigationView{
                List {
                    ForEach(pasienViewModel.daftarPasien) { item in
                        PasienRowView(pasien: item)
                    }
                    .onDelete(perform: pasienViewModel.hapusRow)
                    .onMove(perform: pasienViewModel.pindahRow)
                }
                .navigationTitle("Daftar Pasien")
                .navigationBarItems(leading: EditButton())
            }
            .tabItem {
                Image(systemName: "person.3")
                Text("Pasien")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        DaftarPasienView()
            .environmentObject(PasienViewModel())
    }
}

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif




