//
//  PasienViewModel.swift
//  SOSpital
//
//  Created by Arles Halley on 26/08/21.
//

import Foundation

class PasienViewModel: ObservableObject {
    @Published var daftarPasien: [Pasien] = []
    
    init() {
        getPasien()
    }
    
    func getPasien() {
        let newPasien = [
            Pasien(namaDepan: "Gemma", namaBelakang: "Hunt", kamar: "2409", bed: "A", heartRate: 80, respirationRate: 16, saturation: 98, systolic: 120, diastolic: 80, statusPasien: "Normal"),
            Pasien(namaDepan: "Angel", namaBelakang: "Heart", kamar: "3312", bed: "B", heartRate: 110, respirationRate: 18, saturation: 98, systolic: 110, diastolic: 80, statusPasien: "Gawat"),
            Pasien(namaDepan: "Richard", namaBelakang: "Wijaya", kamar: "1212", bed: "C", heartRate: 100, respirationRate: 25, saturation: 97, systolic: 120, diastolic: 90, statusPasien: "Gawat"),
            Pasien(namaDepan: "Salman", namaBelakang: "Rozi", kamar: "5536", bed: "D", heartRate: 130, respirationRate: 19, saturation: 99, systolic: 125, diastolic: 85, statusPasien: "Darurat"),
            Pasien(namaDepan: "Kit", namaBelakang: "McNamara", kamar: "2134", bed: "E", heartRate: 90, respirationRate: 18, saturation: 92, systolic: 130, diastolic: 90, statusPasien: "Darurat")
        ]
        daftarPasien.append(contentsOf: newPasien)
    }
    
    func hapusRow(indexSet: IndexSet) {
        daftarPasien.remove(atOffsets: indexSet)
    }
    
    func pindahRow(from: IndexSet, to: Int) {
        daftarPasien.move(fromOffsets: from, toOffset: to)
    }
    
    func tambahPasien(namaDepan: String, namaBelakang: String, kamar: String, bed: String, heartRate: Int, respirationRate: Int, saturation: Int, systolic: Int, diastolic: Int, statusPasien: String) {
        let pasienBaru = Pasien(namaDepan: namaDepan, namaBelakang: namaBelakang, kamar: kamar, bed: bed, heartRate: heartRate, respirationRate: respirationRate, saturation: saturation, systolic: systolic, diastolic: diastolic, statusPasien: statusPasien)
        daftarPasien.append(pasienBaru)
    }
    
    func gawat() {
        print("Gawat")
    }
    
    func darurat() {
        print("Darurat")
    }
}
