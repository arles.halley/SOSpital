//
//  Landing Page.swift
//  SOSpital WatchKit Extension
//
//  Created by Helen I H on 26/08/21.
//

import SwiftUI

struct LandingPage: View {
    @State var actionSheet = false
    var body: some View {
        ScrollView {
            Button(action: {
                actionSheet.toggle()
            }, label: {
                VStack(alignment: .leading){
                    Text("Gawat")
                        .font(.headline)
                        .foregroundColor(Color.yellow)
                    Text("Kamar 2409 - Bed A")
                        .font(.body)
                    Text("now")
                        .font(.footnote)
                        .foregroundColor(Color.gray)
                }
            }).actionSheet(isPresented: $actionSheet, content: {
                let button1 = ActionSheet.Button.destructive(Text("Treat"))
                let button2 = ActionSheet.Button.default(Text("Dismiss"))
                
                return ActionSheet(title: Text("Gawat"), message: Text("Kamar 2409 - Bed A Ny. Gemma SpO2"), buttons: [button1, button2])
            })
        }
    }
}

struct LandingPage_Previews: PreviewProvider {
    static var previews: some View {
        LandingPage()
    }
}
