//
//  SOSpitalApp.swift
//  SOSpital WatchKit Extension
//
//  Created by Arles Halley on 24/08/21.
//

import SwiftUI

@main
struct SOSpitalApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                LandingPage()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
