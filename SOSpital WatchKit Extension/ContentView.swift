//
//  ContentView.swift
//  SOSpital WatchKit Extension
//
//  Created by Arles Halley on 24/08/21.
//

import SwiftUI
import UserNotifications

struct ContentView: View {
    @State var show  = false
    var body: some View {
        ZStack{
            Button(action: {
                self.send()
            }){
                Text("")
            }
        }.onAppear{
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name("Success"), object: nil, queue: .main){ (_) in
                self.show = true
            }
            
        }
    }
    func send() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {(_,_) in
            
        }
        let content = UNMutableNotificationContent()
            content.title = "Gawat"
        
        let treat = UNNotificationAction(identifier: "treat", title: "Treat", options: .destructive)
        
        let categories = UNNotificationCategory(identifier: "action", actions: [treat], intentIdentifiers: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([categories])
        content.categoryIdentifier = "action"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let req = UNNotificationRequest(identifier: "req", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(req, withCompletionHandler: nil)
    }

    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
